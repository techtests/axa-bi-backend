import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'

import { API, PORT } from './constants/settings'
import routes from './routes'

const app = express()
const jsonParser = bodyParser.json({ type: 'application/json' })

// Enable CORS
app.use(cors())

app.get(`${API}/clients`, jsonParser, routes.clients)
app.get(`${API}/policies`, jsonParser, routes.policies)

// Start listener on ${PORT}
app.listen(PORT, () => {
  console.log(`[Server] Listening @ ${PORT}`)
})

export default app
