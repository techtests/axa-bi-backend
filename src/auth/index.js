import axios from 'axios'
import { clients } from '../constants/urls'

import { ADMIN } from '../constants/roles'

const outside = (id, level) => {
  return new Promise((resolve, reject) => {
    // Get fresh client list
    axios.get(clients).then(result => {
      // Get the user doing the request by its token (id)
      const user = result.data.clients.filter(client => client.id === id)[0]
      // Check if the user exists, its level is equal or greater than the route level
      if (user && (user.role === level || user.role === ADMIN)) {
        resolve(user.role)
      } else {
        resolve(false)
      }
    }).catch(error => { reject(error) })
  })
}

export default async (id, level) => {
  let access = await outside(id, level).catch(error => {
    console.log(`${error.response.status}: ${error.response.statusText}`)
  })
  return access
}
