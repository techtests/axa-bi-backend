import clients from './clients'
import policies from './policies'

export default {
  clients,
  policies
}
