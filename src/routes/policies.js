import axios from 'axios'

import auth from '../auth'
import { ADMIN } from '../constants/roles'
import { clients, policies } from '../constants/urls'

// Function needs to be async because auth is promise-based
export default async (req, res) => {
  // Id from params
  const { clientName } = req.body
  // Check if the request user has enough level
  const accessRole = await auth(req.query.token, ADMIN)
  // Return 401 if not
  if (!accessRole) {
    res.status(401).end()
  } else {
    // Request client list
    axios.get(policies).then(result => {
      // If the request has an id filter
      if (clientName) {
        // Get all clients
        axios.get(clients).then(clients => {
          const client = clients.data.clients.filter(client => client.name === clientName)[0]
          // Return 404 if client is not found by name
          if (!client) {
            res.status(404).end()
          } else {
            const policiesByClientId = result.data.policies.filter(policy => policy.clientId === client.id)
            // Return 404 if policy is not found by client id
            if (!policiesByClientId) {
              res.status(404).end()
            } else {
              res.json(policiesByClientId)
            }
          }
        })
      // If the request doesn't have a client name, return the whole list.
      } else {
        res.json(result.data.policies)
      }
    // Return the error
    }).catch(error => res.json(error))
  }
}
