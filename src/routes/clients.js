import axios from 'axios'

import auth from '../auth'
import { ADMIN, USER } from '../constants/roles'
import { clients, policies } from '../constants/urls'

// Function needs to be async because auth is promise-based
export default async (req, res) => {
  const { id, name, policyId } = req.body
  // Check if the request user has enough level
  const accessRole = await auth(req.query.token, USER)
  // Return 401 if not
  if (!accessRole) {
    res.status(401).end()
  } else {
    // Request client list
    axios.get(clients).then(result => {
      // If the request has an id filter
      if (id) {
        // Filter the client by id
        const client = result.data.clients.filter(client => client.id === id)[0]
        // Return 404 if the client wasn't found.
        if (!client) {
          res.status(404).end()
        } else {
          res.json(client)
        }
      // If the request doesn't have an id, look up by name
      } else if (name) {
        const client = result.data.clients.filter(client => client.name === name)[0]
        // Return 404 if the client wasn't found.
        if (!client) {
          res.status(404).end()
        } else {
          res.json(client)
        }
      } else if (policyId) {
        // Check accessRole and return 401 if not ADMIN
        if (accessRole !== ADMIN) {
          res.status(401).end()
        } else {
          // Get list of policies
          axios.get(policies).then(policies => {
            const userId = policies.data.policies.filter(policy => policy.id === policyId)[0]
            // Return 404 if the policyId wasn't found.
            if (!userId) {
              res.status(404).end()
            } else {
              const client = result.data.clients.filter(client => client.id === userId.clientId)[0]
              // Return 404 if the client wasn't found.
              if (!client) {
                res.status(404).end()
              } else {
                res.json(client)
              }
            }
          })
        }
      // If the request doesn't have an id, name or policyId, return the whole list.
      } else {
        res.json(result.data.clients)
      }
    // Return the error
    }).catch(error => res.json(error))
  }
}
