# AXA BI
## Backend

As an insurance company we've been asked to develop an application that manages some information about our insurance policies and company clients. To do that, we have two services that provide all the data we need:
- The list of company clients can be found at: http://www.mocky.io/v2/5808862710000087232b75ac
- The list of company policies can be found at: http://www.mocky.io/v2/580891a4100000e8242b75c5

With that information, we need to create a Web API that exposes the following services with some added constraints:
- Get user data filtered by user id -> Can be accessed by users with role "users" and "admin"
- Get user data filtered by user name -> Can be accessed by users with role "users" and "admin"
- Get the list of policies linked to a user name -> Can be accessed by users with role "admin"
- Get the user linked to a policy number -> Can be accessed by users with role "admin"

We have the following constraints:
- Think about licenses of 3d party libraries (if needed)
- Authentication and authorization. Take the user role from the web service that returns the list of company clients

As our stakeholders are very fussy, here you have some tips:
- Usage of last technologies
- Solution properly structured
- Usage of patterns
- Add everything you think it's needed to ensure product's quality
- Documentation

## Technically talking:

- [x] Use the framework you feel more comfortable with: Express has been used. Others libs: axios, cors, body-parser, babel-cli...
- [ ] Testing is possible… unit, e2e… all you can think: Everything is importable and testable. Easily with mocha, chai, chai-http...
- [x] ES6… is a must: Done
- [x] Document… it’s not a joke… README it’s mandatory for any project… even if you are doing it as a hobbie: The README is done, the code is well-commented!
- [x] Error handling… : Data access errors are handled in the backend; Comsumption errors should be handled in the frontend.
- [ ] Logs…
- [ ] Think some way to use a database… that will be a plus: This seems like a middleware or b-to-b program; it feeds from another API so using a database is not needed. We could store clients and policies in a database (like mongoDB) and access it using mongoose, but not needed if we can directly access the fresh API. (We'd lose realtime b2b)
- [x] Last but not least… USE ~~GITHUB~~!!!!: Gitlab ftw!

## Installation & Setup

`npm install && npm start` should do it.

Port is `9028` by default, endpoints are `/api/...` by default, you can change both in `constants/settings.js`

## Endpoints

```/api/clients?token=xxxx``` (token = user.id to check permissions from)
- Accepts `application/json` with "id", "name" and "policyId" properties to filter by.
- Successfull requests will return an object with the filtered client (200).
- An empty body will yield a list with all clients (200).
- Invalid/not found ids or names will return 404.
- Failing to provide a valid token will return 401.
- If the provided token user doesn't have enough permissions, it will return 401.
- id, name properties need USER permissions, policyId properties need ADMIN permissions.

```/api/policies?token=xxxx``` (token = user.id to check permissions from)
- Accepts `application/json` with "name" property to filter by.
- Successfull requests will return an object with the filtered policy (200).
- An empty body will yield a list with all policies (200).
- Invalid/not found ids or names will return 404.
- Failing to provide a valid token will return 401.
- If the provided token user doesn't have enough permissions, it will return 401.
- All properties need ADMIN permissions.